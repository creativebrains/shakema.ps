<?php
ini_set('display_errors','On');
error_reporting(E_ALL ^E_NOTICE);
date_default_timezone_set  ( 'Europe/Vienna'  );

#if( php_sapi_name() != "cli") die('direct!');

// SETTING
define('ROOT', dirname(__FILE__));
include(ROOT . "/config.php");


$con    = new PDO("mysql:host=" . $host . ";dbname=" . $db . ";", $user, $password);

#$sql = "SELECT * FROM `earthquake_0911` WHERE `latitude` >= 29.43737711434627 AND  `latitude` <= 45.719741462352935 AND `longitude` >= 139.44190365625002 AND `longitude` <= 178.37059634374998;";
$sql =  "SELECT * FROM `earthquake` WHERE `datetime` >= :date;";


$find   = $con->prepare($sql);

#strtotime(date("Y-m-d", strtotime($date)) . " +1 week");
$find->bindValue(":date", strtotime(date("Y-m-d 00:00")." -1 year"));
$find->execute();

$txt    = "/*".date("Y-m-d H:i")."*/\nvar earthquakes = [\n";
$i      = 0;

while($item = $find->fetch(PDO::FETCH_OBJ)){

    #pr($item);

    $date = new DateTime($item->datetime);
    //riday, March 18, 2011 08:01:34 UTC
    $date = strtoupper($date->format("l d M Y H:i +0000"));

    $txt .= "\t{\n";
       $txt .= "\t\t\"datetime\": \"".$date."\",\n";
       $txt .= "\t\t\"longitude\": \"".$item->longitude."\",\n";
       $txt .= "\t\t\"latitude\": \"".$item->latitude."\",\n";
       $txt .= "\t\t\"magnitude\": \"".$item->magnitude."\",\n";
       $txt .= "\t\t\"depth\": \"".$item->depth."\",\n";
       $txt .= "\t\t\"region\": \"".substr($item->title,strpos($item->title, " - ")+3)."\"\n";
    $txt .= "},\n\n";
    $i++;
}

$txt = substr($txt,0,-3);
$txt .= "\n];";
echo "found $i";
echo $txt;
file_put_contents(ROOT . "/assets/javascripts/eq.js", $txt);


