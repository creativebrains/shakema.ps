<?php

ini_set('display_errors','On');
error_reporting(E_ALL | E_NOTICE);
date_default_timezone_set  ( 'Europe/Vienna'  );


// SETTING
define("ROOT", dirname(__FILE__));
include(ROOT . "/config.php");


$days   = (int)$_GET["days"];
/*
$sw_lat = (float)$_GET["sw_lat"];
$sw_lng = (float)$_GET["sw_lng"];
$ne_lat = (float)$_GET["ne_lat"];
$ne_lng = (float)$_GET["ne_lng"];
*/



$con    = new PDO("mysql:host=" . $host . ";dbname=" . $db . ";", $user, $password);

//build sql + filter days
$sql    = "SELECT * FROM `earthquake_0911` WHERE ";
if($days != null || $days === 0) {
    $sql .= "`datetime` >= :date";
}

/*
$sql .= "`latitude` >= " . $sw_lat . " AND `latitude` <= " . $ne_lat . " AND ";
$sql .= "`longitude` >= " . $sw_lng . " AND `longitude` <= " . $ne_lng;
*/
$sql .= " ORDER BY `datetime` DESC;";


$find   = $con->prepare($sql);
if($days != null || $days === 0) {
    $str = date("Y-m-d 00:00");
    if($days > 0)
         $str .= " -".$days." days";
    $find->bindValue(":date", date("Y-m-d 00:00",strtotime($str)));
}
$find->execute();


$i = 0;
$eqs = Array();

while($item = $find->fetch(PDO::FETCH_OBJ)){

    #pr($item);

    $date = new DateTime($item->datetime);
    //riday, March 18, 2011 08:01:34 UTC
    $date = strtoupper($date->format("l d M Y H:i +0000"));

    $tmp = new stdClass;
    $tmp->datetime = $date;
    $tmp->longitude = number_format($item->longitude, 3);
    $tmp->latitude = number_format($item->latitude, 3);
    $tmp->magnitude =number_format($item->magnitude, 1);
    $tmp->depth = number_format($item->depth,1);
    $tmp->region = substr($item->title,strpos($item->title, " - ")+3);

    $eqs[] = $tmp;

    $i++;
}


echo json_encode($eqs);


