SHAKEMA.PS
====================

About
---------------------
Shakema.ps shows you the latest earthquakes in Google Map.

It is completely build in Javascript using the Google Maps API and the earthquake-data polling is made in PHP & MySQL.

Included Stats
---------------------
+ Date and Time
+ Magnitude
+ Latitude
+ Longitude
+ Depth in KM
+ Region