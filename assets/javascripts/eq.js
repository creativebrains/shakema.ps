/*2011-09-23 19:05*/
var earthquakes = [
	{
		"datetime": "TUESDAY 22 MAR 2011 22:12 +0000",
		"longitude": "140.67300",
		"latitude": "37.10870",
		"magnitude": "5.7",
		"depth": "1",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 16:12 +0000",
		"longitude": "141.63800",
		"latitude": "35.67170",
		"magnitude": "5.5",
		"depth": "33.3",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 15:03 +0000",
		"longitude": "141.57200",
		"latitude": "35.78640",
		"magnitude": "6.1",
		"depth": "16.4",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 13:50 +0000",
		"longitude": "141.53400",
		"latitude": "35.79100",
		"magnitude": "5.8",
		"depth": "15",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 13:31 +0000",
		"longitude": "-15.97890",
		"latitude": "-33.08520",
		"magnitude": "6.1",
		"depth": "11.9",
		"region": "SOUTHERN MID-ATLANTIC RIDGE"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 12:01 +0000",
		"longitude": "143.20300",
		"latitude": "36.94730",
		"magnitude": "5.6",
		"depth": "1",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 11:21 +0000",
		"longitude": "143.13900",
		"latitude": "39.78950",
		"magnitude": "5.5",
		"depth": "24.8",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 09:44 +0000",
		"longitude": "143.43600",
		"latitude": "39.86290",
		"magnitude": "6.6",
		"depth": "15.5",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 09:19 +0000",
		"longitude": "141.86100",
		"latitude": "37.33400",
		"magnitude": "6.4",
		"depth": "27",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 07:18 +0000",
		"longitude": "143.95600",
		"latitude": "37.24950",
		"magnitude": "6.6",
		"depth": "26.5",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 22 MAR 2011 03:38 +0000",
		"longitude": "141.05300",
		"latitude": "35.22760",
		"magnitude": "5.9",
		"depth": "11.4",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 21 MAR 2011 20:42 +0000",
		"longitude": "-123.41800",
		"latitude": "39.12170",
		"magnitude": "3.5",
		"depth": "5.19",
		"region": "18.3 km (11.4 mi) W of Ukiah, CA"
},

	{
		"datetime": "MONDAY 21 MAR 2011 09:49 +0000",
		"longitude": "70.92040",
		"latitude": "36.51890",
		"magnitude": "5.8",
		"depth": "196.7",
		"region": "HINDU KUSH REGION, AFGHANISTAN"
},

	{
		"datetime": "SUNDAY 20 MAR 2011 12:03 +0000",
		"longitude": "141.95800",
		"latitude": "39.40860",
		"magnitude": "5.8",
		"depth": "45.7",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 20 MAR 2011 08:26 +0000",
		"longitude": "121.25000",
		"latitude": "19.03470",
		"magnitude": "5.9",
		"depth": "31.4",
		"region": "BABUYAN ISLANDS REGION, PHILIPPINES"
},

	{
		"datetime": "SUNDAY 20 MAR 2011 08:17 +0000",
		"longitude": "-122.77200",
		"latitude": "38.78670",
		"magnitude": "3.5",
		"depth": "0.74",
		"region": "3.3 km (2.0 mi) ESE of The Geysers, CA"
},

	{
		"datetime": "SUNDAY 20 MAR 2011 08:00 +0000",
		"longitude": "121.42200",
		"latitude": "22.39570",
		"magnitude": "5.5",
		"depth": "17.3",
		"region": "TAIWAN REGION"
},

	{
		"datetime": "SUNDAY 20 MAR 2011 01:30 +0000",
		"longitude": "141.18200",
		"latitude": "36.94510",
		"magnitude": "5.5",
		"depth": "22.8",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 19 MAR 2011 20:08 +0000",
		"longitude": "38.49910",
		"latitude": "6.67980",
		"magnitude": "5",
		"depth": "14.9",
		"region": "ETHIOPIA"
},

	{
		"datetime": "SATURDAY 19 MAR 2011 09:56 +0000",
		"longitude": "140.37500",
		"latitude": "36.80990",
		"magnitude": "6.1",
		"depth": "24.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 19 MAR 2011 01:22 +0000",
		"longitude": "143.17700",
		"latitude": "39.65900",
		"magnitude": "5.9",
		"depth": "9.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 18 MAR 2011 23:33 +0000",
		"longitude": "142.48600",
		"latitude": "39.14480",
		"magnitude": "5.6",
		"depth": "27.7",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 18 MAR 2011 19:54 +0000",
		"longitude": "-112.07200",
		"latitude": "34.85910",
		"magnitude": "3.7",
		"depth": "4.9",
		"region": "ARIZONA"
},

	{
		"datetime": "FRIDAY 18 MAR 2011 03:23 +0000",
		"longitude": "143.50200",
		"latitude": "37.77130",
		"magnitude": "5.5",
		"depth": "19",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 18:55 +0000",
		"longitude": "142.29100",
		"latitude": "37.17540",
		"magnitude": "5.6",
		"depth": "29.8",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 17:05 +0000",
		"longitude": "-137.78300",
		"latitude": "58.27120",
		"magnitude": "4.12",
		"depth": "29.2639",
		"region": "107.2 miles SW of Haines"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 12:54 +0000",
		"longitude": "141.30700",
		"latitude": "36.76570",
		"magnitude": "5.9",
		"depth": "25.1",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 12:32 +0000",
		"longitude": "140.74400",
		"latitude": "35.52290",
		"magnitude": "5.6",
		"depth": "21",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 08:03 +0000",
		"longitude": "167.89900",
		"latitude": "-17.28310",
		"magnitude": "5.8",
		"depth": "27.8",
		"region": "VANUATU"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 06:12 +0000",
		"longitude": "143.45100",
		"latitude": "37.75490",
		"magnitude": "5.7",
		"depth": "25",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 04:13 +0000",
		"longitude": "142.20400",
		"latitude": "40.19770",
		"magnitude": "6.1",
		"depth": "25.3",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 02:47 +0000",
		"longitude": "167.74300",
		"latitude": "-17.33870",
		"magnitude": "6.3",
		"depth": "15.2",
		"region": "VANUATU"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 02:13 +0000",
		"longitude": "-124.70600",
		"latitude": "40.62820",
		"magnitude": "3.4",
		"depth": "9.45",
		"region": "37.8 km (23.5 mi) W of Ferndale, CA"
},

	{
		"datetime": "WEDNESDAY 16 MAR 2011 23:38 +0000",
		"longitude": "142.66100",
		"latitude": "39.16510",
		"magnitude": "5.7",
		"depth": "25.3",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 16 MAR 2011 17:36 +0000",
		"longitude": "-74.55000",
		"latitude": "45.57000",
		"magnitude": "4.3",
		"depth": "18",
		"region": "ONTARIO-QUEBEC BORDER REGION, CANADA"
},

	{
		"datetime": "WEDNESDAY 16 MAR 2011 06:29 +0000",
		"longitude": "142.07600",
		"latitude": "39.94430",
		"magnitude": "5.8",
		"depth": "31.1",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 16 MAR 2011 03:52 +0000",
		"longitude": "140.80300",
		"latitude": "35.79510",
		"magnitude": "6",
		"depth": "25",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 15 MAR 2011 20:30 +0000",
		"longitude": "141.19300",
		"latitude": "35.25180",
		"magnitude": "5.7",
		"depth": "23.8",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 15 MAR 2011 18:14 +0000",
		"longitude": "-118.72700",
		"latitude": "38.38680",
		"magnitude": "3.66",
		"depth": "13.4159",
		"region": "11.0 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "TUESDAY 15 MAR 2011 15:23 +0000",
		"longitude": "143.27100",
		"latitude": "40.33000",
		"magnitude": "6.1",
		"depth": "19.4",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 15 MAR 2011 13:31 +0000",
		"longitude": "138.70000",
		"latitude": "35.30000",
		"magnitude": "6.2",
		"depth": "10",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 14 MAR 2011 03:15 +0000",
		"longitude": "142.14600",
		"latitude": "36.26520",
		"magnitude": "5.6",
		"depth": "31.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 14 MAR 2011 01:02 +0000",
		"longitude": "140.96500",
		"latitude": "36.45490",
		"magnitude": "5.8",
		"depth": "18.8",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 13 MAR 2011 20:16 +0000",
		"longitude": "-100.81000",
		"latitude": "32.95360",
		"magnitude": "3.8",
		"depth": "5",
		"region": "WESTERN TEXAS"
},

	{
		"datetime": "SUNDAY 13 MAR 2011 17:55 +0000",
		"longitude": "141.03000",
		"latitude": "35.20530",
		"magnitude": "5.5",
		"depth": "24",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 13 MAR 2011 09:52 +0000",
		"longitude": "141.87000",
		"latitude": "38.85510",
		"magnitude": "5.5",
		"depth": "31.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 13 MAR 2011 09:25 +0000",
		"longitude": "142.47500",
		"latitude": "39.05470",
		"magnitude": "5.5",
		"depth": "24.2",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 13 MAR 2011 07:56 +0000",
		"longitude": "143.23700",
		"latitude": "39.64380",
		"magnitude": "5.9",
		"depth": "24.9",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 13 MAR 2011 07:04 +0000",
		"longitude": "144.13100",
		"latitude": "38.32690",
		"magnitude": "5.6",
		"depth": "18.8",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 13 MAR 2011 02:23 +0000",
		"longitude": "142.28700",
		"latitude": "36.35390",
		"magnitude": "6.2",
		"depth": "24.9",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 13 MAR 2011 01:42 +0000",
		"longitude": "142.90800",
		"latitude": "36.94200",
		"magnitude": "5.5",
		"depth": "25.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 13 MAR 2011 01:26 +0000",
		"longitude": "141.73100",
		"latitude": "35.74170",
		"magnitude": "6.2",
		"depth": "24.5",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 22:31 +0000",
		"longitude": "142.33300",
		"latitude": "39.22540",
		"magnitude": "5.6",
		"depth": "25.8",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 22:12 +0000",
		"longitude": "141.95900",
		"latitude": "37.66200",
		"magnitude": "6.3",
		"depth": "14.3",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 21:48 +0000",
		"longitude": "142.56500",
		"latitude": "39.62610",
		"magnitude": "5.5",
		"depth": "24.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 17:19 +0000",
		"longitude": "142.64500",
		"latitude": "36.57290",
		"magnitude": "6",
		"depth": "4.3",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 14:43 +0000",
		"longitude": "142.40600",
		"latitude": "39.47060",
		"magnitude": "5.7",
		"depth": "21.5",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 14:35 +0000",
		"longitude": "141.66000",
		"latitude": "35.78390",
		"magnitude": "5.6",
		"depth": "24.4",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 13:26 +0000",
		"longitude": "-109.93000",
		"latitude": "25.35000",
		"magnitude": "5.5",
		"depth": "17.6",
		"region": "GULF OF CALIFORNIA"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 13:15 +0000",
		"longitude": "141.17500",
		"latitude": "37.26070",
		"magnitude": "6.4",
		"depth": "37.5",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 12:53 +0000",
		"longitude": "143.57300",
		"latitude": "37.75410",
		"magnitude": "5.8",
		"depth": "19.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 11:46 +0000",
		"longitude": "141.65600",
		"latitude": "35.76050",
		"magnitude": "5.7",
		"depth": "17.6",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 10:53 +0000",
		"longitude": "142.35200",
		"latitude": "39.07450",
		"magnitude": "6.1",
		"depth": "24.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 10:20 +0000",
		"longitude": "143.48300",
		"latitude": "37.19730",
		"magnitude": "5.5",
		"depth": "25.5",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 06:18 +0000",
		"longitude": "142.37700",
		"latitude": "39.23490",
		"magnitude": "5.5",
		"depth": "36.4",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 06:10 +0000",
		"longitude": "143.75300",
		"latitude": "38.59440",
		"magnitude": "5.5",
		"depth": "10.4",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 04:04 +0000",
		"longitude": "139.12600",
		"latitude": "-2.99430",
		"magnitude": "5.5",
		"depth": "48.3",
		"region": "NEAR THE NORTH COAST OF PAPUA, INDONESIA"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 03:01 +0000",
		"longitude": "142.77000",
		"latitude": "39.61210",
		"magnitude": "5.8",
		"depth": "25",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 02:47 +0000",
		"longitude": "143.69600",
		"latitude": "37.61720",
		"magnitude": "5.6",
		"depth": "25.7",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 01:47 +0000",
		"longitude": "142.68200",
		"latitude": "37.58790",
		"magnitude": "6.8",
		"depth": "24.8",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 01:19 +0000",
		"longitude": "-173.17400",
		"latitude": "-16.72710",
		"magnitude": "6.1",
		"depth": "10.9",
		"region": "TONGA"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 01:17 +0000",
		"longitude": "-173.06600",
		"latitude": "-16.66090",
		"magnitude": "5.7",
		"depth": "31.5",
		"region": "TONGA"
},

	{
		"datetime": "SATURDAY 12 MAR 2011 00:45 +0000",
		"longitude": "141.81500",
		"latitude": "36.04150",
		"magnitude": "5.5",
		"depth": "23.7",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 22:51 +0000",
		"longitude": "144.96700",
		"latitude": "37.80650",
		"magnitude": "5.8",
		"depth": "25",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 20:23 +0000",
		"longitude": "141.58300",
		"latitude": "35.81820",
		"magnitude": "5.5",
		"depth": "24.2",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 20:11 +0000",
		"longitude": "142.64500",
		"latitude": "39.02540",
		"magnitude": "6.3",
		"depth": "8.7",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 19:46 +0000",
		"longitude": "139.07000",
		"latitude": "40.47200",
		"magnitude": "6.6",
		"depth": "10",
		"region": "NEAR THE WEST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 19:31 +0000",
		"longitude": "138.36700",
		"latitude": "36.96220",
		"magnitude": "5.5",
		"depth": "10.3",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 19:24 +0000",
		"longitude": "140.63900",
		"latitude": "35.76960",
		"magnitude": "5.5",
		"depth": "24.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 19:02 +0000",
		"longitude": "142.90000",
		"latitude": "39.37220",
		"magnitude": "6.1",
		"depth": "24.8",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 18:59 +0000",
		"longitude": "138.35500",
		"latitude": "37.03700",
		"magnitude": "6.2",
		"depth": "10",
		"region": "NEAR THE WEST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 17:16 +0000",
		"longitude": "144.14500",
		"latitude": "37.11100",
		"magnitude": "5.5",
		"depth": "26.4",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 16:11 +0000",
		"longitude": "143.57700",
		"latitude": "39.46330",
		"magnitude": "5.5",
		"depth": "9.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 15:19 +0000",
		"longitude": "141.85500",
		"latitude": "36.23260",
		"magnitude": "5.6",
		"depth": "25",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 15:13 +0000",
		"longitude": "141.79600",
		"latitude": "35.99680",
		"magnitude": "6.2",
		"depth": "18.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 14:00 +0000",
		"longitude": "140.84500",
		"latitude": "36.15060",
		"magnitude": "5.5",
		"depth": "30.8",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 13:43 +0000",
		"longitude": "144.20900",
		"latitude": "38.97240",
		"magnitude": "5.6",
		"depth": "25.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 13:34 +0000",
		"longitude": "141.85000",
		"latitude": "36.24920",
		"magnitude": "5.6",
		"depth": "35.5",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 11:44 +0000",
		"longitude": "142.23100",
		"latitude": "36.70860",
		"magnitude": "5.8",
		"depth": "31",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 11:36 +0000",
		"longitude": "142.52100",
		"latitude": "39.27570",
		"magnitude": "6.5",
		"depth": "11.6",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 11:21 +0000",
		"longitude": "140.91300",
		"latitude": "35.75930",
		"magnitude": "5.7",
		"depth": "25.2",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 11:10 +0000",
		"longitude": "141.85500",
		"latitude": "35.53440",
		"magnitude": "5.5",
		"depth": "27.7",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 11:00 +0000",
		"longitude": "141.48100",
		"latitude": "37.81270",
		"magnitude": "5.6",
		"depth": "28.7",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 10:28 +0000",
		"longitude": "143.53100",
		"latitude": "39.44670",
		"magnitude": "5.9",
		"depth": "29.3",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 10:10 +0000",
		"longitude": "142.77900",
		"latitude": "39.24780",
		"magnitude": "6",
		"depth": "28.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 09:47 +0000",
		"longitude": "142.93800",
		"latitude": "39.68530",
		"magnitude": "5.5",
		"depth": "29.7",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 09:09 +0000",
		"longitude": "143.26700",
		"latitude": "37.71660",
		"magnitude": "5.5",
		"depth": "36.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 08:58 +0000",
		"longitude": "-154.99200",
		"latitude": "19.34050",
		"magnitude": "4.6",
		"depth": "9.3",
		"region": "HAWAII REGION, HAWAII"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 08:40 +0000",
		"longitude": "141.12200",
		"latitude": "37.46530",
		"magnitude": "5.9",
		"depth": "38.6",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 08:31 +0000",
		"longitude": "141.20000",
		"latitude": "37.42750",
		"magnitude": "6.1",
		"depth": "25",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 08:19 +0000",
		"longitude": "142.00000",
		"latitude": "36.20000",
		"magnitude": "6.5",
		"depth": "19.9",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 08:15 +0000",
		"longitude": "144.61100",
		"latitude": "37.03430",
		"magnitude": "6.2",
		"depth": "27.8",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 08:10 +0000",
		"longitude": "140.63100",
		"latitude": "36.39400",
		"magnitude": "5.5",
		"depth": "30.4",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 08:01 +0000",
		"longitude": "142.73400",
		"latitude": "37.07080",
		"magnitude": "5.9",
		"depth": "22.6",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:56 +0000",
		"longitude": "142.30500",
		"latitude": "37.13030",
		"magnitude": "5.6",
		"depth": "34",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:54 +0000",
		"longitude": "141.56500",
		"latitude": "37.74250",
		"magnitude": "5.7",
		"depth": "45.3",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:42 +0000",
		"longitude": "141.91900",
		"latitude": "36.40560",
		"magnitude": "5.8",
		"depth": "29.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:38 +0000",
		"longitude": "142.78300",
		"latitude": "39.24980",
		"magnitude": "5.9",
		"depth": "29.1",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:36 +0000",
		"longitude": "-178.41900",
		"latitude": "51.20870",
		"magnitude": "4.58",
		"depth": "20.6354",
		"region": "88.7 miles WSW of Adak"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:28 +0000",
		"longitude": "141.91100",
		"latitude": "36.80170",
		"magnitude": "6.1",
		"depth": "24",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:25 +0000",
		"longitude": "144.62100",
		"latitude": "37.91620",
		"magnitude": "6.1",
		"depth": "15",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:14 +0000",
		"longitude": "141.81100",
		"latitude": "36.64840",
		"magnitude": "6.3",
		"depth": "25",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:13 +0000",
		"longitude": "142.34700",
		"latitude": "36.05110",
		"magnitude": "5.9",
		"depth": "28.5",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 07:10 +0000",
		"longitude": "142.73400",
		"latitude": "37.89870",
		"magnitude": "5.8",
		"depth": "30",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 06:57 +0000",
		"longitude": "140.99200",
		"latitude": "35.75790",
		"magnitude": "6.3",
		"depth": "30.2",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 06:48 +0000",
		"longitude": "142.76400",
		"latitude": "37.99280",
		"magnitude": "6.3",
		"depth": "22.3",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 06:25 +0000",
		"longitude": "144.55300",
		"latitude": "38.10630",
		"magnitude": "7.7",
		"depth": "19.7",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 06:15 +0000",
		"longitude": "141.17200",
		"latitude": "36.17930",
		"magnitude": "7.9",
		"depth": "39",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 06:07 +0000",
		"longitude": "141.86200",
		"latitude": "36.40140",
		"magnitude": "6.4",
		"depth": "35.4",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 06:06 +0000",
		"longitude": "142.31600",
		"latitude": "39.02510",
		"magnitude": "6.4",
		"depth": "25.1",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 11 MAR 2011 05:46 +0000",
		"longitude": "142.36900",
		"latitude": "38.32200",
		"magnitude": "9",
		"depth": "32",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 10 MAR 2011 17:08 +0000",
		"longitude": "116.76500",
		"latitude": "-6.86240",
		"magnitude": "6.5",
		"depth": "508.1",
		"region": "BALI SEA"
},

	{
		"datetime": "THURSDAY 10 MAR 2011 08:08 +0000",
		"longitude": "143.30300",
		"latitude": "38.63000",
		"magnitude": "5.7",
		"depth": "17.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 10 MAR 2011 04:58 +0000",
		"longitude": "97.99400",
		"latitude": "24.71000",
		"magnitude": "5.4",
		"depth": "10",
		"region": "MYANMAR-CHINA BORDER REGION"
},

	{
		"datetime": "WEDNESDAY 09 MAR 2011 21:24 +0000",
		"longitude": "149.65900",
		"latitude": "-6.02200",
		"magnitude": "6.5",
		"depth": "29",
		"region": "NEW BRITAIN REGION, PAPUA NEW GUINEA"
},

	{
		"datetime": "WEDNESDAY 09 MAR 2011 21:22 +0000",
		"longitude": "142.64100",
		"latitude": "38.38530",
		"magnitude": "6.1",
		"depth": "23",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 09 MAR 2011 20:48 +0000",
		"longitude": "-92.39750",
		"latitude": "35.24330",
		"magnitude": "3.5",
		"depth": "5.9",
		"region": "ARKANSAS"
},

	{
		"datetime": "WEDNESDAY 09 MAR 2011 18:44 +0000",
		"longitude": "143.19900",
		"latitude": "38.50200",
		"magnitude": "6",
		"depth": "23",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 09 MAR 2011 18:16 +0000",
		"longitude": "142.50600",
		"latitude": "38.37800",
		"magnitude": "6.1",
		"depth": "22",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 09 MAR 2011 04:37 +0000",
		"longitude": "142.99100",
		"latitude": "38.66580",
		"magnitude": "5.7",
		"depth": "25.5",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 09 MAR 2011 02:57 +0000",
		"longitude": "142.82500",
		"latitude": "38.40230",
		"magnitude": "5.6",
		"depth": "17.5",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 09 MAR 2011 02:45 +0000",
		"longitude": "142.83600",
		"latitude": "38.42420",
		"magnitude": "7.2",
		"depth": "32",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 07 MAR 2011 00:09 +0000",
		"longitude": "160.73900",
		"latitude": "-10.33400",
		"magnitude": "6.6",
		"depth": "37.9",
		"region": "SOLOMON ISLANDS"
},

	{
		"datetime": "SUNDAY 06 MAR 2011 14:32 +0000",
		"longitude": "-27.01920",
		"latitude": "-56.38660",
		"magnitude": "6.5",
		"depth": "84.2",
		"region": "SOUTH SANDWICH ISLANDS REGION"
},

	{
		"datetime": "SUNDAY 06 MAR 2011 12:31 +0000",
		"longitude": "-69.39070",
		"latitude": "-18.11470",
		"magnitude": "6.2",
		"depth": "101.3",
		"region": "TARAPACA, CHILE"
},

	{
		"datetime": "FRIDAY 04 MAR 2011 04:07 +0000",
		"longitude": "157.34200",
		"latitude": "-8.92770",
		"magnitude": "5.7",
		"depth": "12.7",
		"region": "SOLOMON ISLANDS"
},

	{
		"datetime": "THURSDAY 03 MAR 2011 15:55 +0000",
		"longitude": "-92.38880",
		"latitude": "35.24130",
		"magnitude": "3.5",
		"depth": "6.6",
		"region": "ARKANSAS"
},

	{
		"datetime": "THURSDAY 03 MAR 2011 05:33 +0000",
		"longitude": "-121.54000",
		"latitude": "36.80930",
		"magnitude": "3.4",
		"depth": "6.42",
		"region": "3.9 km (2.4 mi) S of San Juan Bautista, CA"
},

	{
		"datetime": "THURSDAY 03 MAR 2011 02:58 +0000",
		"longitude": "-118.37300",
		"latitude": "37.39930",
		"magnitude": "3.5",
		"depth": "15",
		"region": "4.3 km (2.6 mi) NNE of Bishop, CA"
},

	{
		"datetime": "WEDNESDAY 02 MAR 2011 18:50 +0000",
		"longitude": "-76.90520",
		"latitude": "8.55850",
		"magnitude": "5.6",
		"depth": "39.9",
		"region": "NEAR THE NORTH COAST OF COLOMBIA"
},

	{
		"datetime": "TUESDAY 01 MAR 2011 16:03 +0000",
		"longitude": "-92.39130",
		"latitude": "35.24030",
		"magnitude": "3.3",
		"depth": "5",
		"region": "ARKANSAS"
},

	{
		"datetime": "TUESDAY 01 MAR 2011 15:12 +0000",
		"longitude": "-154.77100",
		"latitude": "56.96870",
		"magnitude": "4.56",
		"depth": "36.6904",
		"region": "104.7 miles WSW of Kodiak"
},

	{
		"datetime": "TUESDAY 01 MAR 2011 02:19 +0000",
		"longitude": "-122.82000",
		"latitude": "38.81530",
		"magnitude": "4.5",
		"depth": "2.95",
		"region": "2.2 km (1.4 mi) NNW of The Geysers, CA"
},

	{
		"datetime": "TUESDAY 01 MAR 2011 00:53 +0000",
		"longitude": "-112.10700",
		"latitude": "-29.60500",
		"magnitude": "6",
		"depth": "10",
		"region": "EASTER ISLAND REGION"
},

	{
		"datetime": "MONDAY 28 FEB 2011 11:04 +0000",
		"longitude": "-16.28800",
		"latitude": "-58.91750",
		"magnitude": "5.6",
		"depth": "10",
		"region": "EAST OF THE SOUTH SANDWICH ISLANDS"
},

	{
		"datetime": "MONDAY 28 FEB 2011 08:46 +0000",
		"longitude": "-92.34150",
		"latitude": "35.28210",
		"magnitude": "3.4",
		"depth": "2.5",
		"region": "ARKANSAS"
},

	{
		"datetime": "MONDAY 28 FEB 2011 05:18 +0000",
		"longitude": "-92.37420",
		"latitude": "35.27060",
		"magnitude": "3.8",
		"depth": "4.2",
		"region": "ARKANSAS"
},

	{
		"datetime": "MONDAY 28 FEB 2011 05:00 +0000",
		"longitude": "-92.34400",
		"latitude": "35.26510",
		"magnitude": "4.7",
		"depth": "3.8",
		"region": "ARKANSAS"
},

	{
		"datetime": "MONDAY 28 FEB 2011 01:29 +0000",
		"longitude": "-73.29600",
		"latitude": "-37.26800",
		"magnitude": "5.8",
		"depth": "20",
		"region": "BIO-BIO, CHILE"
},

	{
		"datetime": "SATURDAY 26 FEB 2011 11:08 +0000",
		"longitude": "-140.57100",
		"latitude": "61.66490",
		"magnitude": "4.09",
		"depth": "2.4893",
		"region": "119.3 miles WNW of Haines Junction"
},

	{
		"datetime": "SATURDAY 26 FEB 2011 02:10 +0000",
		"longitude": "164.94300",
		"latitude": "-10.63340",
		"magnitude": "5.9",
		"depth": "55.5",
		"region": "SANTA CRUZ ISLANDS REGION"
},

	{
		"datetime": "FRIDAY 25 FEB 2011 13:07 +0000",
		"longitude": "-94.97870",
		"latitude": "17.96950",
		"magnitude": "5.7",
		"depth": "132.9",
		"region": "VERACRUZ, MEXICO"
},

	{
		"datetime": "FRIDAY 25 FEB 2011 09:49 +0000",
		"longitude": "-92.37420",
		"latitude": "35.27750",
		"magnitude": "3.5",
		"depth": "5.9",
		"region": "ARKANSAS"
},

	{
		"datetime": "FRIDAY 25 FEB 2011 00:12 +0000",
		"longitude": "-157.55000",
		"latitude": "21.18000",
		"magnitude": "3.6",
		"depth": "19",
		"region": "OAHU REGION, HAWAII"
},

	{
		"datetime": "THURSDAY 24 FEB 2011 20:05 +0000",
		"longitude": "-118.35500",
		"latitude": "36.03680",
		"magnitude": "3.5",
		"depth": "1.45",
		"region": "22.7 mi W of Coso Junction, CA"
},

	{
		"datetime": "WEDNESDAY 23 FEB 2011 04:50 +0000",
		"longitude": "-122.95600",
		"latitude": "39.49380",
		"magnitude": "4.2",
		"depth": "11.26",
		"region": "9.5 km (5.9 mi) N of Lake Pillsbury, CA"
},

	{
		"datetime": "WEDNESDAY 23 FEB 2011 04:49 +0000",
		"longitude": "-122.94900",
		"latitude": "39.49930",
		"magnitude": "4.3",
		"depth": "12.75",
		"region": "10.2 km (6.3 mi) N of Lake Pillsbury, CA"
},

	{
		"datetime": "TUESDAY 22 FEB 2011 23:03 +0000",
		"longitude": "-117.37800",
		"latitude": "37.16200",
		"magnitude": "3.35",
		"depth": "12.2918",
		"region": "38.3 miles WNW of BEATTY-NV"
},

	{
		"datetime": "TUESDAY 22 FEB 2011 14:09 +0000",
		"longitude": "143.86700",
		"latitude": "22.10900",
		"magnitude": "5.7",
		"depth": "100.8",
		"region": "VOLCANO ISLANDS, JAPAN REGION"
},

	{
		"datetime": "TUESDAY 22 FEB 2011 01:50 +0000",
		"longitude": "172.73400",
		"latitude": "-43.58790",
		"magnitude": "5.5",
		"depth": "9.6",
		"region": "SOUTH ISLAND OF NEW ZEALAND"
},

	{
		"datetime": "TUESDAY 22 FEB 2011 00:04 +0000",
		"longitude": "172.79800",
		"latitude": "-43.58020",
		"magnitude": "5.6",
		"depth": "6.7",
		"region": "SOUTH ISLAND OF NEW ZEALAND"
},

	{
		"datetime": "MONDAY 21 FEB 2011 23:51 +0000",
		"longitude": "172.71000",
		"latitude": "-43.60000",
		"magnitude": "6.3",
		"depth": "5",
		"region": "SOUTH ISLAND OF NEW ZEALAND"
},

	{
		"datetime": "MONDAY 21 FEB 2011 10:57 +0000",
		"longitude": "178.43900",
		"latitude": "-26.08300",
		"magnitude": "6.4",
		"depth": "561.8",
		"region": "SOUTH OF THE FIJI ISLANDS"
},

	{
		"datetime": "MONDAY 21 FEB 2011 06:58 +0000",
		"longitude": "-64.76370",
		"latitude": "-27.20490",
		"magnitude": "5.6",
		"depth": "10.6",
		"region": "SANTIAGO DEL ESTERO, ARGENTINA"
},

	{
		"datetime": "SUNDAY 27 MAR 2011 06:20 +0000",
		"longitude": "-92.34590",
		"latitude": "14.44860",
		"magnitude": "5.7",
		"depth": "44.9",
		"region": "OFFSHORE CHIAPAS, MEXICO"
},

	{
		"datetime": "SATURDAY 26 MAR 2011 22:49 +0000",
		"longitude": "-179.49400",
		"latitude": "-15.78850",
		"magnitude": "5.9",
		"depth": "13.4",
		"region": "FIJI REGION"
},

	{
		"datetime": "FRIDAY 25 MAR 2011 18:00 +0000",
		"longitude": "-179.60900",
		"latitude": "50.97720",
		"magnitude": "4.45",
		"depth": "47.8427",
		"region": "141.6 miles WSW of Adak"
},

	{
		"datetime": "FRIDAY 25 MAR 2011 11:36 +0000",
		"longitude": "141.94200",
		"latitude": "38.76330",
		"magnitude": "6.4",
		"depth": "39.2",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 24 MAR 2011 21:45 +0000",
		"longitude": "-120.07100",
		"latitude": "40.02730",
		"magnitude": "3.2",
		"depth": "2.66",
		"region": "26.0 km (16.1 mi) NNE of Chilcoot-Vinton, CA"
},

	{
		"datetime": "THURSDAY 24 MAR 2011 21:20 +0000",
		"longitude": "-120.06200",
		"latitude": "40.03880",
		"magnitude": "3.5",
		"depth": "5.97",
		"region": "27.4 km (17.0 mi) NNE of Chilcoot-Vinton, CA"
},

	{
		"datetime": "THURSDAY 24 MAR 2011 13:55 +0000",
		"longitude": "99.94900",
		"latitude": "20.70490",
		"magnitude": "6.8",
		"depth": "10",
		"region": "MYANMAR"
},

	{
		"datetime": "THURSDAY 24 MAR 2011 08:21 +0000",
		"longitude": "142.15500",
		"latitude": "39.11990",
		"magnitude": "6.1",
		"depth": "36.8",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 17 MAR 2011 17:05 +0000",
		"longitude": "-137.80500",
		"latitude": "58.26810",
		"magnitude": "4.17",
		"depth": "19.4406",
		"region": "107.9 miles SW of Haines"
},

	{
		"datetime": "SUNDAY 27 MAR 2011 13:41 +0000",
		"longitude": "142.36000",
		"latitude": "38.99860",
		"magnitude": "5.6",
		"depth": "39",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 27 MAR 2011 13:41 +0000",
		"longitude": "142.32000",
		"latitude": "38.94030",
		"magnitude": "5.6",
		"depth": "48.6",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 27 MAR 2011 22:23 +0000",
		"longitude": "142.12000",
		"latitude": "38.40540",
		"magnitude": "6.5",
		"depth": "5.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 27 MAR 2011 22:23 +0000",
		"longitude": "142.10200",
		"latitude": "38.40160",
		"magnitude": "6.1",
		"depth": "17.1",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 28 MAR 2011 05:44 +0000",
		"longitude": "141.72000",
		"latitude": "36.37240",
		"magnitude": "5.5",
		"depth": "18.8",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 28 MAR 2011 04:32 +0000",
		"longitude": "-115.09400",
		"latitude": "32.26230",
		"magnitude": "4",
		"depth": "36.84",
		"region": "36.5 mi SE of Calexico, CA"
},

	{
		"datetime": "MONDAY 28 MAR 2011 09:12 +0000",
		"longitude": "-100.88300",
		"latitude": "32.89190",
		"magnitude": "3.7",
		"depth": "5",
		"region": "WESTERN TEXAS"
},

	{
		"datetime": "MONDAY 28 MAR 2011 09:12 +0000",
		"longitude": "-100.88300",
		"latitude": "32.89190",
		"magnitude": "2.9",
		"depth": "5",
		"region": "WESTERN TEXAS"
},

	{
		"datetime": "MONDAY 28 MAR 2011 04:32 +0000",
		"longitude": "-115.17600",
		"latitude": "32.34330",
		"magnitude": "4",
		"depth": "15",
		"region": "29.2 mi SE of Calexico, CA"
},

	{
		"datetime": "TUESDAY 29 MAR 2011 10:43 +0000",
		"longitude": "-116.75800",
		"latitude": "33.22270",
		"magnitude": "3.8",
		"depth": "9.02",
		"region": "1.1 mi S of Lake Henshaw, CA"
},

	{
		"datetime": "TUESDAY 29 MAR 2011 10:54 +0000",
		"longitude": "142.25100",
		"latitude": "37.41100",
		"magnitude": "6.3",
		"depth": "18.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 29 MAR 2011 10:54 +0000",
		"longitude": "142.27000",
		"latitude": "37.41720",
		"magnitude": "6.1",
		"depth": "13.8",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 29 MAR 2011 10:43 +0000",
		"longitude": "-116.75700",
		"latitude": "33.22480",
		"magnitude": "3.7",
		"depth": "11.26",
		"region": "1.0 mi SSE of Lake Henshaw, CA"
},

	{
		"datetime": "TUESDAY 29 MAR 2011 16:35 +0000",
		"longitude": "143.48500",
		"latitude": "39.61170",
		"magnitude": "5.5",
		"depth": "5.6",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 29 MAR 2011 16:04 +0000",
		"longitude": "-76.95000",
		"latitude": "46.55900",
		"magnitude": "3.5",
		"depth": "18",
		"region": "SOUTHERN QUEBEC, CANADA"
},

	{
		"datetime": "TUESDAY 29 MAR 2011 16:35 +0000",
		"longitude": "143.54800",
		"latitude": "39.60470",
		"magnitude": "5.5",
		"depth": "5.7",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 29 MAR 2011 16:35 +0000",
		"longitude": "143.46600",
		"latitude": "39.59170",
		"magnitude": "5.5",
		"depth": "15.7",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 30 MAR 2011 05:29 +0000",
		"longitude": "142.43300",
		"latitude": "36.13480",
		"magnitude": "6",
		"depth": "30.3",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 25 MAR 2011 18:00 +0000",
		"longitude": "-179.50700",
		"latitude": "50.85170",
		"magnitude": "4.5",
		"depth": "26.9314",
		"region": "141.9 miles WSW of Adak"
},

	{
		"datetime": "FRIDAY 25 MAR 2011 18:00 +0000",
		"longitude": "-179.49500",
		"latitude": "50.85320",
		"magnitude": "4.5",
		"depth": "25.8796",
		"region": "141.4 miles WSW of Adak"
},

	{
		"datetime": "THURSDAY 31 MAR 2011 00:11 +0000",
		"longitude": "-177.52700",
		"latitude": "-16.54750",
		"magnitude": "5.9",
		"depth": "13.4",
		"region": "FIJI REGION"
},

	{
		"datetime": "THURSDAY 31 MAR 2011 00:11 +0000",
		"longitude": "-177.49200",
		"latitude": "-16.56630",
		"magnitude": "6.4",
		"depth": "23.7",
		"region": "FIJI REGION"
},

	{
		"datetime": "THURSDAY 31 MAR 2011 07:15 +0000",
		"longitude": "142.01700",
		"latitude": "38.95360",
		"magnitude": "6.2",
		"depth": "39.6",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 31 MAR 2011 11:58 +0000",
		"longitude": "-124.25200",
		"latitude": "40.44900",
		"magnitude": "3.5",
		"depth": "29.02",
		"region": "13.5 km (8.4 mi) WSW of Rio Dell, CA"
},

	{
		"datetime": "THURSDAY 31 MAR 2011 11:58 +0000",
		"longitude": "-124.25200",
		"latitude": "40.44900",
		"magnitude": "3.6",
		"depth": "29.02",
		"region": "13.5 km (8.4 mi) WSW of Rio Dell, CA"
},

	{
		"datetime": "THURSDAY 31 MAR 2011 20:22 +0000",
		"longitude": "-115.21900",
		"latitude": "32.37470",
		"magnitude": "4.3",
		"depth": "0.06",
		"region": "25.9 mi SE of Calexico, CA"
},

	{
		"datetime": "THURSDAY 31 MAR 2011 20:22 +0000",
		"longitude": "-115.22800",
		"latitude": "32.33920",
		"magnitude": "4.3",
		"depth": "10.69",
		"region": "27.5 mi SE of Calexico, CA"
},

	{
		"datetime": "THURSDAY 31 MAR 2011 11:58 +0000",
		"longitude": "-124.21800",
		"latitude": "40.42730",
		"magnitude": "3.6",
		"depth": "25.75",
		"region": "12.4 km (7.7 mi) SW of Rio Dell, CA"
},

	{
		"datetime": "FRIDAY 01 APR 2011 11:57 +0000",
		"longitude": "141.98200",
		"latitude": "39.35290",
		"magnitude": "5.9",
		"depth": "38.1",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 01 APR 2011 12:56 +0000",
		"longitude": "-110.34300",
		"latitude": "43.02680",
		"magnitude": "4.1",
		"depth": "5.1",
		"region": "WYOMING"
},

	{
		"datetime": "FRIDAY 01 APR 2011 13:29 +0000",
		"longitude": "26.45460",
		"latitude": "35.72870",
		"magnitude": "5.9",
		"depth": "77.9",
		"region": "CRETE, GREECE"
},

	{
		"datetime": "FRIDAY 01 APR 2011 14:44 +0000",
		"longitude": "141.67300",
		"latitude": "36.20910",
		"magnitude": "5.5",
		"depth": "33.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 01 APR 2011 17:03 +0000",
		"longitude": "143.17800",
		"latitude": "40.35630",
		"magnitude": "5.5",
		"depth": "20.5",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 02 APR 2011 06:05 +0000",
		"longitude": "153.81300",
		"latitude": "-5.64260",
		"magnitude": "5.5",
		"depth": "71.9",
		"region": "NEW IRELAND REGION, PAPUA NEW GUINEA"
},

	{
		"datetime": "SATURDAY 02 APR 2011 10:59 +0000",
		"longitude": "-69.07180",
		"latitude": "-19.55550",
		"magnitude": "5.9",
		"depth": "83.4",
		"region": "TARAPACA, CHILE"
},

	{
		"datetime": "SATURDAY 02 APR 2011 18:32 +0000",
		"longitude": "-157.13300",
		"latitude": "56.20040",
		"magnitude": "4.79",
		"depth": "45.8726",
		"region": "143.5 miles ENE of Sand Point"
},

	{
		"datetime": "SATURDAY 02 APR 2011 18:32 +0000",
		"longitude": "-156.90100",
		"latitude": "55.97570",
		"magnitude": "4.69",
		"depth": "23.8563",
		"region": "146.7 miles ENE of Sand Point"
},

	{
		"datetime": "SATURDAY 02 APR 2011 20:09 +0000",
		"longitude": "-155.25200",
		"latitude": "19.35650",
		"magnitude": "3.5",
		"depth": "1.8",
		"region": "ISLAND OF HAWAII, HAWAII"
},

	{
		"datetime": "SUNDAY 03 APR 2011 14:07 +0000",
		"longitude": "-178.60300",
		"latitude": "-17.62150",
		"magnitude": "6.2",
		"depth": "530.6",
		"region": "FIJI REGION"
},

	{
		"datetime": "SUNDAY 03 APR 2011 14:07 +0000",
		"longitude": "-178.58300",
		"latitude": "-17.60350",
		"magnitude": "6.4",
		"depth": "555.5",
		"region": "FIJI REGION"
},

	{
		"datetime": "SUNDAY 03 APR 2011 15:30 +0000",
		"longitude": "-116.12500",
		"latitude": "31.74850",
		"magnitude": "3.5",
		"depth": "9.65",
		"region": "68.6 mi S of Ocotillo, CA"
},

	{
		"datetime": "SUNDAY 03 APR 2011 14:07 +0000",
		"longitude": "-178.54900",
		"latitude": "-17.61290",
		"magnitude": "6.4",
		"depth": "552.7",
		"region": "FIJI REGION"
},

	{
		"datetime": "SUNDAY 03 APR 2011 14:07 +0000",
		"longitude": "-178.57800",
		"latitude": "-17.64930",
		"magnitude": "6.4",
		"depth": "551.9",
		"region": "FIJI REGION"
},

	{
		"datetime": "SUNDAY 03 APR 2011 20:06 +0000",
		"longitude": "107.74800",
		"latitude": "-9.78640",
		"magnitude": "6.7",
		"depth": "24",
		"region": "SOUTH OF JAVA, INDONESIA"
},

	{
		"datetime": "MONDAY 04 APR 2011 11:31 +0000",
		"longitude": "80.75020",
		"latitude": "29.67810",
		"magnitude": "5.4",
		"depth": "12.5",
		"region": "NEPAL-INDIA BORDER REGION"
},

	{
		"datetime": "MONDAY 04 APR 2011 18:20 +0000",
		"longitude": "-153.07200",
		"latitude": "58.96690",
		"magnitude": "4.6",
		"depth": "66.8977",
		"region": "58.2 miles WSW of Seldovia"
},

	{
		"datetime": "SATURDAY 02 APR 2011 20:09 +0000",
		"longitude": "-155.25400",
		"latitude": "19.34280",
		"magnitude": "3.6",
		"depth": "0.2",
		"region": "ISLAND OF HAWAII, HAWAII"
},

	{
		"datetime": "MONDAY 04 APR 2011 18:20 +0000",
		"longitude": "-153.13000",
		"latitude": "58.92190",
		"magnitude": "4.46",
		"depth": "69.5341",
		"region": "61.7 miles SW of Seldovia"
},

	{
		"datetime": "TUESDAY 05 APR 2011 00:26 +0000",
		"longitude": "-148.22400",
		"latitude": "62.47290",
		"magnitude": "4.07",
		"depth": "35.222",
		"region": "61.2 miles E of Talkeetna"
},

	{
		"datetime": "TUESDAY 05 APR 2011 04:10 +0000",
		"longitude": "-178.52400",
		"latitude": "-17.63260",
		"magnitude": "5.7",
		"depth": "565.5",
		"region": "FIJI REGION"
},

	{
		"datetime": "TUESDAY 05 APR 2011 07:05 +0000",
		"longitude": "-112.09800",
		"latitude": "44.61680",
		"magnitude": "4.1",
		"depth": "5",
		"region": "WESTERN MONTANA"
},

	{
		"datetime": "TUESDAY 05 APR 2011 11:14 +0000",
		"longitude": "126.97300",
		"latitude": "3.06070",
		"magnitude": "6",
		"depth": "17.6",
		"region": "KEPULAUAN TALAUD, INDONESIA"
},

	{
		"datetime": "TUESDAY 05 APR 2011 11:56 +0000",
		"longitude": "-121.62400",
		"latitude": "36.07130",
		"magnitude": "4.2",
		"depth": "9.65",
		"region": "7.7 km (4.8 mi) NW of Lopez Point, CA"
},

	{
		"datetime": "TUESDAY 05 APR 2011 11:56 +0000",
		"longitude": "-121.62400",
		"latitude": "36.07130",
		"magnitude": "3.9",
		"depth": "9.65",
		"region": "7.7 km (4.8 mi) NW of Lopez Point, CA"
},

	{
		"datetime": "TUESDAY 05 APR 2011 11:56 +0000",
		"longitude": "-121.62400",
		"latitude": "36.07200",
		"magnitude": "3.9",
		"depth": "10.72",
		"region": "7.8 km (4.8 mi) NW of Lopez Point, CA"
},

	{
		"datetime": "WEDNESDAY 06 APR 2011 14:01 +0000",
		"longitude": "97.14660",
		"latitude": "1.67490",
		"magnitude": "5.8",
		"depth": "18.9",
		"region": "NIAS REGION, INDONESIA"
},

	{
		"datetime": "WEDNESDAY 06 APR 2011 13:54 +0000",
		"longitude": "141.46900",
		"latitude": "37.63530",
		"magnitude": "5.5",
		"depth": "54.2",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 06 APR 2011 14:01 +0000",
		"longitude": "97.12230",
		"latitude": "1.58740",
		"magnitude": "5.8",
		"depth": "19.1",
		"region": "NIAS REGION, INDONESIA"
},

	{
		"datetime": "WEDNESDAY 06 APR 2011 14:01 +0000",
		"longitude": "97.08840",
		"latitude": "1.61220",
		"magnitude": "5.8",
		"depth": "24.7",
		"region": "NIAS REGION, INDONESIA"
},

	{
		"datetime": "WEDNESDAY 06 APR 2011 13:54 +0000",
		"longitude": "141.46500",
		"latitude": "37.64740",
		"magnitude": "5.4",
		"depth": "56.2",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 06 APR 2011 13:54 +0000",
		"longitude": "141.40700",
		"latitude": "37.65030",
		"magnitude": "5.4",
		"depth": "58.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 07 APR 2011 02:34 +0000",
		"longitude": "-92.35330",
		"latitude": "35.19680",
		"magnitude": "3.6",
		"depth": "2.8",
		"region": "ARKANSAS"
},

	{
		"datetime": "THURSDAY 07 APR 2011 02:34 +0000",
		"longitude": "-92.36570",
		"latitude": "35.23930",
		"magnitude": "3.6",
		"depth": "3.8",
		"region": "ARKANSAS"
},

	{
		"datetime": "THURSDAY 07 APR 2011 13:11 +0000",
		"longitude": "-93.97840",
		"latitude": "17.43110",
		"magnitude": "6.5",
		"depth": "167.4",
		"region": "VERACRUZ, MEXICO"
},

	{
		"datetime": "THURSDAY 07 APR 2011 02:34 +0000",
		"longitude": "-92.36670",
		"latitude": "35.23180",
		"magnitude": "3.4",
		"depth": "6.3",
		"region": "ARKANSAS"
},

	{
		"datetime": "THURSDAY 07 APR 2011 14:32 +0000",
		"longitude": "141.64000",
		"latitude": "38.25270",
		"magnitude": "7.4",
		"depth": "25.6",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 07 APR 2011 14:32 +0000",
		"longitude": "141.64000",
		"latitude": "38.25300",
		"magnitude": "7.1",
		"depth": "49",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 07 APR 2011 20:41 +0000",
		"longitude": "-85.09060",
		"latitude": "17.21750",
		"magnitude": "5.8",
		"depth": "19.5",
		"region": "NORTH OF HONDURAS"
},

	{
		"datetime": "SATURDAY 02 APR 2011 18:32 +0000",
		"longitude": "-156.89900",
		"latitude": "55.97480",
		"magnitude": "4.69",
		"depth": "24.3317",
		"region": "146.8 miles ENE of Sand Point"
},

	{
		"datetime": "THURSDAY 07 APR 2011 23:11 +0000",
		"longitude": "-92.39140",
		"latitude": "35.21540",
		"magnitude": "4",
		"depth": "2.6",
		"region": "ARKANSAS"
},

	{
		"datetime": "THURSDAY 07 APR 2011 23:11 +0000",
		"longitude": "-92.37230",
		"latitude": "35.25010",
		"magnitude": "4",
		"depth": "6.1",
		"region": "ARKANSAS"
},

	{
		"datetime": "THURSDAY 07 APR 2011 23:11 +0000",
		"longitude": "-92.37270",
		"latitude": "35.25040",
		"magnitude": "3.9",
		"depth": "6.1",
		"region": "ARKANSAS"
},

	{
		"datetime": "FRIDAY 08 APR 2011 14:56 +0000",
		"longitude": "-92.35150",
		"latitude": "35.25380",
		"magnitude": "4.2",
		"depth": "5",
		"region": "ARKANSAS"
},

	{
		"datetime": "FRIDAY 08 APR 2011 14:56 +0000",
		"longitude": "-92.36030",
		"latitude": "35.26280",
		"magnitude": "4.2",
		"depth": "6.7",
		"region": "ARKANSAS"
},

	{
		"datetime": "FRIDAY 08 APR 2011 16:46 +0000",
		"longitude": "-92.36830",
		"latitude": "35.23900",
		"magnitude": "3.5",
		"depth": "4.9",
		"region": "ARKANSAS"
},

	{
		"datetime": "FRIDAY 08 APR 2011 16:46 +0000",
		"longitude": "-92.37580",
		"latitude": "35.25760",
		"magnitude": "3.5",
		"depth": "6.2",
		"region": "ARKANSAS"
},

	{
		"datetime": "FRIDAY 08 APR 2011 14:56 +0000",
		"longitude": "-92.36250",
		"latitude": "35.26100",
		"magnitude": "3.9",
		"depth": "6.3",
		"region": "ARKANSAS"
},

	{
		"datetime": "MONDAY 04 APR 2011 18:20 +0000",
		"longitude": "-153.13200",
		"latitude": "58.92250",
		"magnitude": "4.54",
		"depth": "69.4381",
		"region": "61.7 miles SW of Seldovia"
},

	{
		"datetime": "SATURDAY 09 APR 2011 08:58 +0000",
		"longitude": "-115.72000",
		"latitude": "32.60370",
		"magnitude": "4.1",
		"depth": "10.25",
		"region": "13.9 mi WSW of Calexico, CA"
},

	{
		"datetime": "SATURDAY 09 APR 2011 12:57 +0000",
		"longitude": "131.81100",
		"latitude": "30.01270",
		"magnitude": "6.1",
		"depth": "21.3",
		"region": "KYUSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 05 APR 2011 00:26 +0000",
		"longitude": "-148.22600",
		"latitude": "62.47090",
		"magnitude": "4.05",
		"depth": "39.6673",
		"region": "61.1 miles E of Talkeetna"
},

	{
		"datetime": "SUNDAY 10 APR 2011 16:21 +0000",
		"longitude": "-125.42100",
		"latitude": "40.37850",
		"magnitude": "3.6",
		"depth": "5.13",
		"region": "96.4 km (59.9 mi) W of Petrolia, CA"
},

	{
		"datetime": "SUNDAY 10 APR 2011 23:06 +0000",
		"longitude": "-118.78400",
		"latitude": "38.36220",
		"magnitude": "3.41",
		"depth": "0",
		"region": "14.1 miles SW of HAWTHORNE-NV"
},

	{
		"datetime": "SUNDAY 10 APR 2011 23:06 +0000",
		"longitude": "-118.74100",
		"latitude": "38.37410",
		"magnitude": "3.47",
		"depth": "9.6938",
		"region": "12.2 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "MONDAY 11 APR 2011 00:13 +0000",
		"longitude": "-118.74600",
		"latitude": "38.33700",
		"magnitude": "3.3",
		"depth": "10.1977",
		"region": "14.5 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "MONDAY 11 APR 2011 00:20 +0000",
		"longitude": "-118.74100",
		"latitude": "38.36320",
		"magnitude": "3.53",
		"depth": "9.4156",
		"region": "12.8 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "MONDAY 11 APR 2011 00:21 +0000",
		"longitude": "-118.73900",
		"latitude": "38.37450",
		"magnitude": "4",
		"depth": "15.2563",
		"region": "12.1 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "MONDAY 11 APR 2011 01:05 +0000",
		"longitude": "-118.74500",
		"latitude": "38.37230",
		"magnitude": "3.28",
		"depth": "0",
		"region": "12.4 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "MONDAY 11 APR 2011 00:22 +0000",
		"longitude": "-118.73500",
		"latitude": "38.37940",
		"magnitude": "4.23",
		"depth": "13.7997",
		"region": "11.7 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "MONDAY 11 APR 2011 01:05 +0000",
		"longitude": "-118.73900",
		"latitude": "38.37510",
		"magnitude": "3.28",
		"depth": "5.9836",
		"region": "12.1 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "MONDAY 11 APR 2011 08:16 +0000",
		"longitude": "140.47700",
		"latitude": "37.00690",
		"magnitude": "7.1",
		"depth": "13.1",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 11 APR 2011 09:03 +0000",
		"longitude": "-115.89900",
		"latitude": "31.79380",
		"magnitude": "3.5",
		"depth": "0.24",
		"region": "64.9 mi SSW of Calexico, CA"
},

	{
		"datetime": "MONDAY 11 APR 2011 08:16 +0000",
		"longitude": "140.47700",
		"latitude": "37.00700",
		"magnitude": "6.6",
		"depth": "10",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 11 APR 2011 11:42 +0000",
		"longitude": "140.45400",
		"latitude": "36.96920",
		"magnitude": "5.6",
		"depth": "13.8",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 11 APR 2011 12:36 +0000",
		"longitude": "-115.98100",
		"latitude": "31.67250",
		"magnitude": "3.7",
		"depth": "9.14",
		"region": "73.4 mi S of Ocotillo, CA"
},

	{
		"datetime": "SATURDAY 09 APR 2011 08:21 +0000",
		"longitude": "-178.61000",
		"latitude": "51.70060",
		"magnitude": "3.94",
		"depth": "3.2983",
		"region": "84.4 miles W of Adak"
},

	{
		"datetime": "MONDAY 11 APR 2011 23:08 +0000",
		"longitude": "140.54200",
		"latitude": "35.40550",
		"magnitude": "6.4",
		"depth": "13.1",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "MONDAY 11 APR 2011 23:08 +0000",
		"longitude": "140.54200",
		"latitude": "35.40600",
		"magnitude": "6.2",
		"depth": "13.1",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 12 APR 2011 05:07 +0000",
		"longitude": "140.57900",
		"latitude": "37.11200",
		"magnitude": "6",
		"depth": "10.6",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 12 APR 2011 05:07 +0000",
		"longitude": "140.70000",
		"latitude": "37.00000",
		"magnitude": "6",
		"depth": "10.6",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "TUESDAY 12 APR 2011 17:01 +0000",
		"longitude": "-115.88000",
		"latitude": "31.60550",
		"magnitude": "3.6",
		"depth": "8.34",
		"region": "76.8 mi SSW of Calexico, CA"
},

	{
		"datetime": "TUESDAY 12 APR 2011 19:37 +0000",
		"longitude": "141.82000",
		"latitude": "39.43010",
		"magnitude": "5.8",
		"depth": "44.8",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "SUNDAY 10 APR 2011 16:21 +0000",
		"longitude": "-125.90100",
		"latitude": "40.45670",
		"magnitude": "3.7",
		"depth": "21.19",
		"region": "137.6 km (85.5 mi) W of Petrolia, CA"
},

	{
		"datetime": "WEDNESDAY 13 APR 2011 01:57 +0000",
		"longitude": "-116.42600",
		"latitude": "38.29400",
		"magnitude": "3.26",
		"depth": "3.9747",
		"region": "7.8 miles NNW of WARM SPRINGS-NV"
},

	{
		"datetime": "WEDNESDAY 13 APR 2011 19:57 +0000",
		"longitude": "143.35700",
		"latitude": "39.58710",
		"magnitude": "6.1",
		"depth": "11.2",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 13 APR 2011 20:32 +0000",
		"longitude": "143.19900",
		"latitude": "39.66760",
		"magnitude": "5.7",
		"depth": "20.9",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "WEDNESDAY 13 APR 2011 04:28 +0000",
		"longitude": "-64.26390",
		"latitude": "18.95800",
		"magnitude": "5.1",
		"depth": "31",
		"region": "Virgin Islands region"
},

	{
		"datetime": "WEDNESDAY 13 APR 2011 22:10 +0000",
		"longitude": "-118.74800",
		"latitude": "38.37150",
		"magnitude": "4.43",
		"depth": "13.0664",
		"region": "12.5 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "WEDNESDAY 13 APR 2011 22:16 +0000",
		"longitude": "-118.74200",
		"latitude": "38.38570",
		"magnitude": "4.19",
		"depth": "15.4054",
		"region": "11.5 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "WEDNESDAY 13 APR 2011 22:15 +0000",
		"longitude": "-118.75300",
		"latitude": "38.37890",
		"magnitude": "3.58",
		"depth": "12.0113",
		"region": "12.2 miles SW of HAWTHORNE-NV"
},

	{
		"datetime": "THURSDAY 14 APR 2011 05:45 +0000",
		"longitude": "-178.57400",
		"latitude": "-32.96270",
		"magnitude": "5.7",
		"depth": "24.2",
		"region": "SOUTH OF THE KERMADEC ISLANDS"
},

	{
		"datetime": "THURSDAY 14 APR 2011 06:08 +0000",
		"longitude": "141.87200",
		"latitude": "35.67510",
		"magnitude": "5.7",
		"depth": "14.4",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 14 APR 2011 20:50 +0000",
		"longitude": "-86.25420",
		"latitude": "11.31310",
		"magnitude": "5.9",
		"depth": "56.2",
		"region": "NEAR THE COAST OF NICARAGUA"
},

	{
		"datetime": "FRIDAY 15 APR 2011 02:06 +0000",
		"longitude": "-173.23700",
		"latitude": "-15.27380",
		"magnitude": "5.9",
		"depth": "10.3",
		"region": "TONGA"
},

	{
		"datetime": "FRIDAY 15 APR 2011 07:41 +0000",
		"longitude": "-118.73100",
		"latitude": "38.40310",
		"magnitude": "3.35",
		"depth": "14.8133",
		"region": "10.2 miles SW of HAWTHORNE-NV"
},

	{
		"datetime": "FRIDAY 15 APR 2011 07:41 +0000",
		"longitude": "-118.73000",
		"latitude": "38.40320",
		"magnitude": "3.35",
		"depth": "15.4573",
		"region": "10.1 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "FRIDAY 15 APR 2011 13:33 +0000",
		"longitude": "-118.73900",
		"latitude": "38.38120",
		"magnitude": "3.94",
		"depth": "0",
		"region": "11.7 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "FRIDAY 15 APR 2011 13:33 +0000",
		"longitude": "-118.73200",
		"latitude": "38.39390",
		"magnitude": "4.3",
		"depth": "14.1972",
		"region": "10.7 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "FRIDAY 15 APR 2011 13:33 +0000",
		"longitude": "-118.73500",
		"latitude": "38.39450",
		"magnitude": "4.3",
		"depth": "12.7992",
		"region": "10.8 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "FRIDAY 15 APR 2011 17:06 +0000",
		"longitude": "-118.73900",
		"latitude": "38.40000",
		"magnitude": "3.67",
		"depth": "0",
		"region": "10.6 miles SW of HAWTHORNE-NV"
},

	{
		"datetime": "FRIDAY 15 APR 2011 17:06 +0000",
		"longitude": "-118.73400",
		"latitude": "38.40020",
		"magnitude": "3.89",
		"depth": "15.6176",
		"region": "10.5 miles SW of HAWTHORNE-NV"
},

	{
		"datetime": "FRIDAY 15 APR 2011 17:24 +0000",
		"longitude": "-118.74200",
		"latitude": "38.39530",
		"magnitude": "3.97",
		"depth": "15.8736",
		"region": "11.0 miles SW of HAWTHORNE-NV"
},

	{
		"datetime": "FRIDAY 15 APR 2011 18:21 +0000",
		"longitude": "-118.75300",
		"latitude": "38.39140",
		"magnitude": "4.02",
		"depth": "9.9524",
		"region": "11.5 miles SW of HAWTHORNE-NV"
},

	{
		"datetime": "THURSDAY 14 APR 2011 09:32 +0000",
		"longitude": "-175.23600",
		"latitude": "51.70370",
		"magnitude": "4.13",
		"depth": "38.4082",
		"region": "61.8 miles E of Adak"
},

	{
		"datetime": "FRIDAY 15 APR 2011 21:46 +0000",
		"longitude": "-175.29400",
		"latitude": "-18.59870",
		"magnitude": "5.7",
		"depth": "216.5",
		"region": "TONGA"
},

	{
		"datetime": "THURSDAY 14 APR 2011 09:32 +0000",
		"longitude": "-175.25300",
		"latitude": "51.58010",
		"magnitude": "4.21",
		"depth": "25.7551",
		"region": "63.4 miles ESE of Adak"
},

	{
		"datetime": "THURSDAY 14 APR 2011 09:32 +0000",
		"longitude": "-175.26200",
		"latitude": "51.54850",
		"magnitude": "4.15",
		"depth": "25.9693",
		"region": "63.8 miles ESE of Adak"
},

	{
		"datetime": "SATURDAY 16 APR 2011 01:11 +0000",
		"longitude": "124.02000",
		"latitude": "25.39260",
		"magnitude": "5.7",
		"depth": "131.9",
		"region": "NORTHEAST OF TAIWAN"
},

	{
		"datetime": "SATURDAY 16 APR 2011 01:32 +0000",
		"longitude": "-146.84000",
		"latitude": "61.33040",
		"magnitude": "3.8",
		"depth": "20.0446",
		"region": "21.8 miles NW of Valdez"
},

	{
		"datetime": "SATURDAY 16 APR 2011 01:32 +0000",
		"longitude": "-146.70300",
		"latitude": "61.35900",
		"magnitude": "3.61",
		"depth": "33.2259",
		"region": "20.1 miles NW of Valdez"
},

	{
		"datetime": "SATURDAY 16 APR 2011 02:19 +0000",
		"longitude": "139.64900",
		"latitude": "36.41110",
		"magnitude": "5.8",
		"depth": "20.1",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 16 APR 2011 02:19 +0000",
		"longitude": "139.68400",
		"latitude": "36.39490",
		"magnitude": "5.8",
		"depth": "59.3",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 16 APR 2011 07:06 +0000",
		"longitude": "-161.53100",
		"latitude": "55.57280",
		"magnitude": "4.23",
		"depth": "14.5702",
		"region": "43.9 miles WNW of Sand Point"
},

	{
		"datetime": "SATURDAY 16 APR 2011 08:34 +0000",
		"longitude": "-161.66500",
		"latitude": "55.66050",
		"magnitude": "3.87",
		"depth": "12.6918",
		"region": "48.7 miles NNE of King Cove"
},

	{
		"datetime": "SATURDAY 16 APR 2011 20:36 +0000",
		"longitude": "143.93400",
		"latitude": "36.88480",
		"magnitude": "5.5",
		"depth": "33.4",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 16 APR 2011 07:06 +0000",
		"longitude": "-161.47200",
		"latitude": "55.57910",
		"magnitude": "4.4",
		"depth": "9.4836",
		"region": "41.9 miles WNW of Sand Point"
},

	{
		"datetime": "SUNDAY 17 APR 2011 00:57 +0000",
		"longitude": "-118.73900",
		"latitude": "38.36240",
		"magnitude": "3.47",
		"depth": "0",
		"region": "12.8 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "SUNDAY 17 APR 2011 00:45 +0000",
		"longitude": "-118.73500",
		"latitude": "38.39430",
		"magnitude": "4.56",
		"depth": "15.3613",
		"region": "10.8 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "SUNDAY 17 APR 2011 01:19 +0000",
		"longitude": "-118.85100",
		"latitude": "38.34300",
		"magnitude": "3.31",
		"depth": "0",
		"region": "17.5 miles SW of HAWTHORNE-NV"
},

	{
		"datetime": "SUNDAY 17 APR 2011 00:55 +0000",
		"longitude": "-118.72800",
		"latitude": "38.36900",
		"magnitude": "4.07",
		"depth": "13.164",
		"region": "12.1 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "SUNDAY 17 APR 2011 00:57 +0000",
		"longitude": "-118.74000",
		"latitude": "38.36190",
		"magnitude": "3.55",
		"depth": "14.5785",
		"region": "12.9 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "SUNDAY 17 APR 2011 01:19 +0000",
		"longitude": "-118.74100",
		"latitude": "38.40100",
		"magnitude": "3.66",
		"depth": "11.757",
		"region": "10.6 miles SW of HAWTHORNE-NV"
},

	{
		"datetime": "SUNDAY 17 APR 2011 01:58 +0000",
		"longitude": "-63.27070",
		"latitude": "-27.59040",
		"magnitude": "5.6",
		"depth": "566.7",
		"region": "SANTIAGO DEL ESTERO, ARGENTINA"
},

	{
		"datetime": "SUNDAY 17 APR 2011 01:05 +0000",
		"longitude": "-118.72200",
		"latitude": "38.39100",
		"magnitude": "3.28",
		"depth": "16.4574",
		"region": "10.7 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "SUNDAY 17 APR 2011 01:05 +0000",
		"longitude": "-118.72200",
		"latitude": "38.39240",
		"magnitude": "3.28",
		"depth": "16.6375",
		"region": "10.6 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "SUNDAY 17 APR 2011 01:58 +0000",
		"longitude": "-63.24990",
		"latitude": "-27.58860",
		"magnitude": "5.6",
		"depth": "562.7",
		"region": "SANTIAGO DEL ESTERO, ARGENTINA"
},

	{
		"datetime": "SUNDAY 17 APR 2011 01:58 +0000",
		"longitude": "-63.25000",
		"latitude": "-27.58900",
		"magnitude": "5.9",
		"depth": "562.7",
		"region": "SANTIAGO DEL ESTERO, ARGENTINA"
},

	{
		"datetime": "SUNDAY 17 APR 2011 07:51 +0000",
		"longitude": "-161.02900",
		"latitude": "55.32640",
		"magnitude": "4.57",
		"depth": "0",
		"region": "21.1 miles W of Sand Point"
},

	{
		"datetime": "SUNDAY 17 APR 2011 07:51 +0000",
		"longitude": "-161.47500",
		"latitude": "55.52790",
		"magnitude": "4.5",
		"depth": "9.7323",
		"region": "40.7 miles WNW of Sand Point"
},

	{
		"datetime": "MONDAY 18 APR 2011 13:02 +0000",
		"longitude": "179.88300",
		"latitude": "-34.16180",
		"magnitude": "6.4",
		"depth": "8.4",
		"region": "SOUTH OF THE KERMADEC ISLANDS"
},

	{
		"datetime": "MONDAY 18 APR 2011 13:03 +0000",
		"longitude": "179.99400",
		"latitude": "-34.32140",
		"magnitude": "6.6",
		"depth": "90.4",
		"region": "SOUTH OF THE KERMADEC ISLANDS"
},

	{
		"datetime": "MONDAY 18 APR 2011 13:03 +0000",
		"longitude": "179.93700",
		"latitude": "-34.31920",
		"magnitude": "6.6",
		"depth": "91.6",
		"region": "SOUTH OF THE KERMADEC ISLANDS"
},

	{
		"datetime": "MONDAY 18 APR 2011 13:03 +0000",
		"longitude": "179.85400",
		"latitude": "-34.34900",
		"magnitude": "6.6",
		"depth": "90.7",
		"region": "SOUTH OF THE KERMADEC ISLANDS"
},

	{
		"datetime": "MONDAY 18 APR 2011 21:57 +0000",
		"longitude": "-122.45500",
		"latitude": "37.59750",
		"magnitude": "3.8",
		"depth": "13.58",
		"region": "3.9 km (2.4 mi) SE of Pacifica, CA"
},

	{
		"datetime": "MONDAY 18 APR 2011 21:57 +0000",
		"longitude": "-122.45500",
		"latitude": "37.59750",
		"magnitude": "3.4",
		"depth": "13.58",
		"region": "3.9 km (2.4 mi) SE of Pacifica, CA"
},

	{
		"datetime": "MONDAY 18 APR 2011 21:57 +0000",
		"longitude": "-122.45000",
		"latitude": "37.60100",
		"magnitude": "3.7",
		"depth": "12.48",
		"region": "3.5 km (2.2 mi) SW of San Bruno, CA"
},

	{
		"datetime": "MONDAY 18 APR 2011 21:57 +0000",
		"longitude": "-122.45000",
		"latitude": "37.60100",
		"magnitude": "3.4",
		"depth": "12.46",
		"region": "3.5 km (2.2 mi) SW of San Bruno, CA"
},

	{
		"datetime": "TUESDAY 19 APR 2011 23:29 +0000",
		"longitude": "-16.05650",
		"latitude": "-44.40980",
		"magnitude": "5.6",
		"depth": "16.5",
		"region": "SOUTHERN MID-ATLANTIC RIDGE"
},

	{
		"datetime": "SUNDAY 17 APR 2011 07:51 +0000",
		"longitude": "-161.50600",
		"latitude": "55.57140",
		"magnitude": "4.5",
		"depth": "12.1015",
		"region": "42.9 miles WNW of Sand Point"
},

	{
		"datetime": "WEDNESDAY 20 APR 2011 13:36 +0000",
		"longitude": "-147.14400",
		"latitude": "60.75350",
		"magnitude": "3.83",
		"depth": "13.4231",
		"region": "37.2 miles SW of Valdez"
},

	{
		"datetime": "WEDNESDAY 20 APR 2011 13:36 +0000",
		"longitude": "-147.14500",
		"latitude": "60.75240",
		"magnitude": "3.83",
		"depth": "13.2693",
		"region": "37.3 miles SW of Valdez"
},

	{
		"datetime": "WEDNESDAY 20 APR 2011 13:36 +0000",
		"longitude": "-147.11900",
		"latitude": "60.80820",
		"magnitude": "3.66",
		"depth": "12.4285",
		"region": "34.0 miles SW of Valdez"
},

	{
		"datetime": "SUNDAY 17 APR 2011 07:51 +0000",
		"longitude": "-161.47800",
		"latitude": "55.53290",
		"magnitude": "4.5",
		"depth": "10.0566",
		"region": "41.0 miles WNW of Sand Point"
},

	{
		"datetime": "THURSDAY 21 APR 2011 00:39 +0000",
		"longitude": "143.47900",
		"latitude": "40.49750",
		"magnitude": "5.8",
		"depth": "15",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 21 APR 2011 01:54 +0000",
		"longitude": "143.67600",
		"latitude": "40.31880",
		"magnitude": "6",
		"depth": "3.7",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 21 APR 2011 01:54 +0000",
		"longitude": "143.62900",
		"latitude": "40.30590",
		"magnitude": "6",
		"depth": "7",
		"region": "OFF THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 21 APR 2011 04:11 +0000",
		"longitude": "-115.55000",
		"latitude": "32.36470",
		"magnitude": "4",
		"depth": "5.28",
		"region": "21.2 mi S of Calexico, CA"
},

	{
		"datetime": "THURSDAY 21 APR 2011 13:37 +0000",
		"longitude": "140.45200",
		"latitude": "35.61680",
		"magnitude": "6.1",
		"depth": "42.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 21 APR 2011 16:11 +0000",
		"longitude": "141.26800",
		"latitude": "37.50820",
		"magnitude": "5.6",
		"depth": "36.6",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 22 APR 2011 00:06 +0000",
		"longitude": "-130.89800",
		"latitude": "63.97730",
		"magnitude": "5.06",
		"depth": "32.6249",
		"region": "254.7 miles E of Dawson City"
},

	{
		"datetime": "THURSDAY 21 APR 2011 04:11 +0000",
		"longitude": "-115.55100",
		"latitude": "32.38750",
		"magnitude": "4",
		"depth": "10",
		"region": "19.7 mi S of Calexico, CA"
},

	{
		"datetime": "FRIDAY 22 APR 2011 15:25 +0000",
		"longitude": "140.98100",
		"latitude": "37.22410",
		"magnitude": "5.5",
		"depth": "35.8",
		"region": "EASTERN HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 21 APR 2011 16:11 +0000",
		"longitude": "141.37100",
		"latitude": "37.52940",
		"magnitude": "5.7",
		"depth": "55.3",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "FRIDAY 22 APR 2011 17:14 +0000",
		"longitude": "-174.63000",
		"latitude": "-22.70320",
		"magnitude": "5.7",
		"depth": "1",
		"region": "TONGA REGION"
},

	{
		"datetime": "SATURDAY 16 APR 2011 07:06 +0000",
		"longitude": "-161.48000",
		"latitude": "55.56000",
		"magnitude": "4.4",
		"depth": "9.3759",
		"region": "41.7 miles WNW of Sand Point"
},

	{
		"datetime": "SATURDAY 23 APR 2011 04:16 +0000",
		"longitude": "161.23300",
		"latitude": "-10.34920",
		"magnitude": "6.9",
		"depth": "81.6",
		"region": "SOLOMON ISLANDS"
},

	{
		"datetime": "SATURDAY 23 APR 2011 10:12 +0000",
		"longitude": "142.89200",
		"latitude": "39.16420",
		"magnitude": "6",
		"depth": "38.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "SATURDAY 23 APR 2011 20:49 +0000",
		"longitude": "-116.32300",
		"latitude": "33.01500",
		"magnitude": "3.6",
		"depth": "3.5",
		"region": "14.2 mi SW of Ocotillo Wells, CA"
},

	{
		"datetime": "SUNDAY 24 APR 2011 23:07 +0000",
		"longitude": "122.80800",
		"latitude": "-4.59250",
		"magnitude": "6.2",
		"depth": "9.4",
		"region": "SULAWESI, INDONESIA"
},

	{
		"datetime": "MONDAY 25 APR 2011 14:17 +0000",
		"longitude": "-158.32200",
		"latitude": "55.75100",
		"magnitude": "3.91",
		"depth": "15.2914",
		"region": "89.5 miles ENE of Sand Point"
},

	{
		"datetime": "MONDAY 25 APR 2011 14:17 +0000",
		"longitude": "-158.04300",
		"latitude": "55.62310",
		"magnitude": "3.69",
		"depth": "1",
		"region": "97.9 miles ENE of Sand Point"
},

	{
		"datetime": "WEDNESDAY 20 APR 2011 13:36 +0000",
		"longitude": "-147.12400",
		"latitude": "60.79970",
		"magnitude": "3.65",
		"depth": "9.7505",
		"region": "34.6 miles SW of Valdez"
},

	{
		"datetime": "MONDAY 25 APR 2011 19:29 +0000",
		"longitude": "-152.49900",
		"latitude": "59.09330",
		"magnitude": "4.97",
		"depth": "56.1249",
		"region": "36.7 miles SW of Seldovia"
},

	{
		"datetime": "MONDAY 25 APR 2011 19:29 +0000",
		"longitude": "-152.58200",
		"latitude": "59.06320",
		"magnitude": "4.97",
		"depth": "61.7265",
		"region": "40.3 miles SW of Seldovia"
},

	{
		"datetime": "TUESDAY 26 APR 2011 11:07 +0000",
		"longitude": "-99.45440",
		"latitude": "17.00170",
		"magnitude": "5.7",
		"depth": "20.6",
		"region": "GUERRERO, MEXICO"
},

	{
		"datetime": "TUESDAY 26 APR 2011 11:07 +0000",
		"longitude": "-99.38050",
		"latitude": "17.10010",
		"magnitude": "5.3",
		"depth": "19.5",
		"region": "GUERRERO, MEXICO"
},

	{
		"datetime": "TUESDAY 26 APR 2011 17:43 +0000",
		"longitude": "-122.81700",
		"latitude": "38.81280",
		"magnitude": "3.5",
		"depth": "3.2",
		"region": "1.9 km (1.2 mi) NNW of The Geysers, CA"
},

	{
		"datetime": "TUESDAY 26 APR 2011 17:43 +0000",
		"longitude": "-122.81700",
		"latitude": "38.81280",
		"magnitude": "3.9",
		"depth": "3.2",
		"region": "1.9 km (1.2 mi) NNW of The Geysers, CA"
},

	{
		"datetime": "TUESDAY 26 APR 2011 21:10 +0000",
		"longitude": "-111.59800",
		"latitude": "27.43910",
		"magnitude": "5.6",
		"depth": "15.1",
		"region": "GULF OF CALIFORNIA"
},

	{
		"datetime": "TUESDAY 26 APR 2011 21:43 +0000",
		"longitude": "125.61300",
		"latitude": "-8.98830",
		"magnitude": "5.6",
		"depth": "1.1",
		"region": "EAST TIMOR REGION"
},

	{
		"datetime": "TUESDAY 26 APR 2011 17:43 +0000",
		"longitude": "-122.81700",
		"latitude": "38.81270",
		"magnitude": "3.9",
		"depth": "3.45",
		"region": "1.8 km (1.1 mi) NNW of The Geysers, CA"
},

	{
		"datetime": "TUESDAY 26 APR 2011 21:43 +0000",
		"longitude": "125.67000",
		"latitude": "-8.90610",
		"magnitude": "5.6",
		"depth": "24.5",
		"region": "EAST TIMOR REGION"
},

	{
		"datetime": "WEDNESDAY 27 APR 2011 19:19 +0000",
		"longitude": "-118.70600",
		"latitude": "38.41750",
		"magnitude": "3.96",
		"depth": "5.05",
		"region": "8.6 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "WEDNESDAY 27 APR 2011 19:19 +0000",
		"longitude": "-118.71800",
		"latitude": "38.41410",
		"magnitude": "4.3",
		"depth": "2.57",
		"region": "9.2 miles SSW of HAWTHORNE-NV"
},

	{
		"datetime": "THURSDAY 28 APR 2011 01:03 +0000",
		"longitude": "-105.72900",
		"latitude": "30.77510",
		"magnitude": "4.3",
		"depth": "10.2",
		"region": "CHIHUAHUA, MEXICO"
},

	{
		"datetime": "MONDAY 25 APR 2011 19:29 +0000",
		"longitude": "-152.52500",
		"latitude": "59.05390",
		"magnitude": "4.97",
		"depth": "56.1859",
		"region": "39.2 miles SW of Seldovia"
},

	{
		"datetime": "MONDAY 25 APR 2011 14:17 +0000",
		"longitude": "-158.05000",
		"latitude": "55.66350",
		"magnitude": "3.74",
		"depth": "13.2677",
		"region": "98.2 miles ENE of Sand Point"
},

	{
		"datetime": "THURSDAY 28 APR 2011 04:58 +0000",
		"longitude": "-105.76200",
		"latitude": "30.70430",
		"magnitude": "4.1",
		"depth": "10",
		"region": "CHIHUAHUA, MEXICO"
},

	{
		"datetime": "THURSDAY 28 APR 2011 07:42 +0000",
		"longitude": "160.53500",
		"latitude": "-9.98460",
		"magnitude": "5.9",
		"depth": "10.3",
		"region": "SOLOMON ISLANDS"
},

	{
		"datetime": "THURSDAY 28 APR 2011 09:27 +0000",
		"longitude": "141.65200",
		"latitude": "37.49240",
		"magnitude": "5.8",
		"depth": "48.9",
		"region": "NEAR THE EAST COAST OF HONSHU, JAPAN"
},

	{
		"datetime": "THURSDAY 28 APR 2011 13:24 +0000",
		"longitude": "-165.23500",
		"latitude": "53.94310",
		"magnitude": "3.86",
		"depth": "52.9356",
		"region": "25.4 miles ESE of Akutan"
},

	{
		"datetime": "THURSDAY 28 APR 2011 13:24 +0000",
		"longitude": "-165.23500",
		"latitude": "53.93630",
		"magnitude": "3.87",
		"depth": "53.0839",
		"region": "25.7 miles ESE of Akutan"
},

	{
		"datetime": "MONDAY 25 APR 2011 19:29 +0000",
		"longitude": "-152.53200",
		"latitude": "59.06160",
		"magnitude": "4.97",
		"depth": "57.3513",
		"region": "39.0 miles SW of Seldovia"
},

	{
		"datetime": "FRIDAY 29 APR 2011 08:56 +0000",
		"longitude": "95.84750",
		"latitude": "3.97210",
		"magnitude": "5.6",
		"depth": "64.4",
		"region": "OFF THE WEST COAST OF NORTHERN SUMATRA"
},

	{
		"datetime": "FRIDAY 29 APR 2011 08:56 +0000",
		"longitude": "95.84710",
		"latitude": "4.02570",
		"magnitude": "5.4",
		"depth": "53.8",
		"region": "NORTHERN SUMATRA, INDONESIA"
},

	{
		"datetime": "THURSDAY 28 APR 2011 13:07 +0000",
		"longitude": "-103.60300",
		"latitude": "10.17450",
		"magnitude": "5.8",
		"depth": "11.7",
		"region": "NORTHERN EAST PACIFIC RISE"
},

	{
		"datetime": "FRIDAY 29 APR 2011 13:12 +0000",
		"longitude": "121.98000",
		"latitude": "21.18200",
		"magnitude": "5.5",
		"depth": "176.7",
		"region": "TAIWAN REGION"
},

	{
		"datetime": "SATURDAY 30 APR 2011 05:45 +0000",
		"longitude": "148.62200",
		"latitude": "-3.26320",
		"magnitude": "5.5",
		"depth": "10.2",
		"region": "BISMARCK SEA"
},

	{
		"datetime": "SATURDAY 30 APR 2011 08:19 +0000",
		"longitude": "-82.28120",
		"latitude": "6.82760",
		"magnitude": "6.1",
		"depth": "10",
		"region": "SOUTH OF PANAMA"
},

	{
		"datetime": "SATURDAY 30 APR 2011 08:19 +0000",
		"longitude": "-82.28660",
		"latitude": "6.87840",
		"magnitude": "6",
		"depth": "9.9",
		"region": "SOUTH OF PANAMA"
},

	{
		"datetime": "SUNDAY 01 MAY 2011 16:12 +0000",
		"longitude": "155.80400",
		"latitude": "-6.99400",
		"magnitude": "5.8",
		"depth": "58.1",
		"region": "BOUGAINVILLE REGION, PAPUA NEW GUINEA"
},

	{
		"datetime": "SUNDAY 01 MAY 2011 16:12 +0000",
		"longitude": "155.81800",
		"latitude": "-6.97760",
		"magnitude": "5.8",
		"depth": "56.6",
		"region": "BOUGAINVILLE REGION, PAPUA NEW GUINEA"
},

	{
		"datetime": "SUNDAY 01 MAY 2011 16:12 +0000",
		"longitude": "155.85100",
		"latitude": "-6.96100",
		"magnitude": "5.8",
		"depth": "54.6",
		"region": "BOUGAINVILLE REGION, PAPUA NEW GUINEA"
},

	{
		"datetime": "MONDAY 02 MAY 2011 11:43 +0000",
		"longitude": "-105.69500",
		"latitude": "30.79440",
		"magnitude": "4.3",
		"depth": "9.8",
		"region": "CHIHUAHUA, MEXICO"
},

	{
		"datetime": "MONDAY 02 MAY 2011 13:55 +0000",
		"longitude": "-105.67400",
		"latitude": "30.72950",
		"magnitude": "4.4",
		"depth": "10",
		"region": "CHIHUAHUA, MEXICO"
},

	{
		"datetime": "THURSDAY 28 APR 2011 13:24 +0000",
		"longitude": "-165.01800",
		"latitude": "53.77910",
		"magnitude": "4.3",
		"depth": "54.2579",
		"region": "39.2 miles SE of Akutan"
},

	{
		"datetime": "MONDAY 02 MAY 2011 19:07 +0000",
		"longitude": "-100.77700",
		"latitude": "33.06780",
		"magnitude": "3.9",
		"depth": "5",
		"region": "WESTERN TEXAS"
},

	{
		"datetime": "SUNDAY 01 MAY 2011 04:13 +0000",
		"longitude": "-119.25700",
		"latitude": "46.40460",
		"magnitude": "3.3",
		"depth": "2.64",
		"region": "13.8 km   N of Richland, WA"
},

	{
		"datetime": "MONDAY 02 MAY 2011 23:55 +0000",
		"longitude": "-87.56850",
		"latitude": "12.13640",
		"magnitude": "5.5",
		"depth": "39.5",
		"region": "NEAR THE COAST OF NICARAGUA"
},

	{
		"datetime": "MONDAY 02 MAY 2011 23:55 +0000",
		"longitude": "-87.48230",
		"latitude": "12.14190",
		"magnitude": "5.5",
		"depth": "25.2",
		"region": "NEAR THE COAST OF NICARAGUA"
},

	{
		"datetime": "TUESDAY 03 MAY 2011 02:58 +0000",
		"longitude": "-105.76700",
		"latitude": "30.67680",
		"magnitude": "4.2",
		"depth": "10.6",
		"region": "CHIHUAHUA, MEXICO"
},

	{
		"datetime": "TUESDAY 03 MAY 2011 21:41 +0000",
		"longitude": "-117.87800",
		"latitude": "36.39580",
		"magnitude": "4.5",
		"depth": "1.99",
		"region": "10.6 mi NE of Olancha, CA"
}
];