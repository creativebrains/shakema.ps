
var earthquake = {
    /* INTERNAL SETTINGS */
    stdCenter: new google.maps.LatLng( 36.011498199779005, 136.23389584375002 ),
    map_div: 'map-canvas',

    /* ESSENTIALS */
    map: null,
    geoMarker: null,
    init_event: null,

    /*  eq */
    eq: [],
    min_magnitude: 0,
    days: 365*10,


    init: function() {

        earthquake.initInterface();

        earthquake.map = new google.maps.Map( document.getElementById(earthquake.map_div), {
            zoom:6,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            disableDefaultUI: false,
            streetViewControl: false,
            streetViewControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
            },
            center: earthquake.stdCenter
        });

        /* OVERLAY*/
        google.maps.event.addListener(earthquake.map, 'click', function(e) {
            
            /* 
            console.log( e.latLng.lat(), e.latLng.lng() );

            var ne = earthquake.map.getBounds().getNorthEast();
            var sw = earthquake.map.getBounds().getSouthWest();

            console.log(sw.lat(), sw.lng());
            console.log(ne.lat(), ne.lng());

            console.log("-----------------------");
            */

            $('#overlay').hide();
        });

        $('#overlay').bind('click', function(e) {
            $('#overlay').hide();
        });

        earthquake.init_event = google.maps.event.addListener(earthquake.map, 'tilesloaded', function(e) {
            earthquake.loadEarthquakes(earthquake.days);
            google.maps.event.removeListener(earthquake.init_event);
        });

        google.maps.event.addListener(earthquake.map, 'dragend', function(e) {
            earthquake.loadEarthquakes(earthquake.days);
        });

        
    },

   initInterface: function() {

        /*Geolocation from the GPS symbol*/
        $('a.icon_gps').bind('click', function(){
            earthquake.findGeolocation();
        });

        /*Search Input*/
        $('input[name=search]').bind('keydown', function(e){
            if(e.keyCode == 13) {
                earthquake.centerAddress($(this).val());
            }
        });
        var search_txt = $('input[name=search]').val();
        $('input[name=search]').bind('click', function(e){
            if(search_txt == $(this).val())
                $(this).val('');
            else
                $(this).select();
        });

        /* jQuery to toggle the alltime/twoweeks buttons */
        $(".cb-enable").bind('click', function(){
            var parent = $(this).parents('.switch');
            $('.cb-disable',parent).removeClass('selected');
            $(this).addClass('selected');
            $('.checkbox',parent).attr('checked', true);earthquake.loadEarthquakes(7);

            earthquake.flushEarthquakes();
            earthquake.loadEarthquakes('all');
        });

        $(".cb-disable").bind('click', function(){
            var parent = $(this).parents('.switch');
            $('.cb-enable',p = arent).removeClass('selected');
            $(this).addClass('selected');
            $('.checkbox',parent).attr('checked', false);

            earthquake.flushEarthquakes();
            earthquake.loadEarthquakes('lastweek');
        });

    },

    centerAddress: function(address) {
        geocoder = new google.maps.Geocoder();
        if (geocoder) {
          geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

              earthquake.setGeoMarker(results[0].geometry.location, false);

            } else {
              alert("Geocode was not successful for the following reason: " + status);
            }
          });
        }
    },


    findGeolocation: function() {

        if(navigator.geolocation) {
            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function(position) {

                initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                earthquake.setGeoMarker(initialLocation, true);

            }, function() {
                earthquake.handleNoGeolocation(browserSupportFlag);
            });
        } else if (google.gears) {
            browserSupportFlag = true;
            var geo = google.gears.factory.create('beta.geolocation');
            geo.getCurrentPosition(function(position) {

                initialLocation = new google.maps.LatLng(position.latitude,position.longitude);
                earthquake.setGeoMarker(initialLocation, true);

            }, function() {
                earthquake.handleNoGeolocation(browserSupportFlag);
            });
        } else {
            browserSupportFlag = false;
            earthquake.handleNoGeolocation(browserSupportFlag);
        }
    },


    setGeoMarker: function( coords, geolocation ) {
        earthquake.map.setCenter( coords );
        earthquake.map.setZoom(10);

        if(earthquake.geoMarker != null)
            earthquake.geoMarker.setMap(null);

        if(geolocation) {

            var icon = new google.maps.MarkerImage(
                'assets/images/ui/geo.png',
                new google.maps.Size(95, 34),
                new google.maps.Point(0, 0),
                new google.maps.Point(46, 34)
            );
        } else {
            var icon = new google.maps.MarkerImage(
                'assets/images/ui/pin.png',
                new google.maps.Size(32, 75),
                new google.maps.Point(0, 0),
                new google.maps.Point(16, 37)
            );
        }

        earthquake.geoMarker = new google.maps.Marker({
            position: coords,
            map: earthquake.map,
            icon: icon
        });
    },

    handleNoGeolocation: function( flag ) {
        var contentString = null;
        var initialLocation = earthquake.stdCenter

        if (errorFlag == true) {
            contentString = "Error: The Geolocation service failed.";
        } else {
            contentString = "Error: Your browser doesn't support geolocation.";
        }

        earthquake.map.setCenter(initialLocation);
        alert(contentString);
    },


    displayOverlay: function(eq) {
        var info = eq.info;

        $('#overlay').css('left', (window.innerWidth/2 - $('#overlay').width()/2)+'px');
        $('#overlay').css('top', (window.innerHeight/2 - $('#overlay').height()/2)+'px');

        var date = info.datetime.replace(/,/g, "").split(" ");

        $('#overlay .date .column2 .small1').html(date[2]+" "+(date[1].substr(0,3).toUpperCase())+" "+date[3]);
        $('#overlay .date .column2 .small2').html(date[date.length-2]);

        $('#overlay .magnitude .column2').html(info.magnitude);
        $('#overlay .latitude .column2').html(info.latitude);
        $('#overlay .longitude .column2').html(info.longitude);
        $('#overlay .depth .column2').html(info.depth);
        $('#overlay .region .column2 .small3').html(info.region);

        $('#overlay').fadeIn(100);

    },


     loadEarthquakes: function(days) {

        earthquake.flushEarthquakes();



        var cnt = earthquakes.length;
        console.log("EQ's found: " + cnt);

        for(var i = 0; i < cnt; i++) {
            earthquake.addEarthquake(earthquakes[i]);
        }


    },



    flushEarthquakes: function() {
        var cnt = earthquake.eq.length;
        for(var i = 0; i < cnt; i++) {
            earthquake.eq[i].setMap(null);
            earthquake.eq[i] = null;
        }
        earthquake.eq.length = 0;

    },



    checkEarthquake: function(eq) {
        var coords = new google.maps.LatLng(eq.info.latitude, eq.info.longitude);

        if( parseFloat(eq.info.magnitude) >= earthquake.min_magnitude && earthquake.map.getBounds().contains(coords) ) {
            eq.setMap(earthquake.map);
            return 1;
        } else {
            eq.setMap(null);
            return 0;
        }
    },



    addEarthquake: function(eq) {

        var color = '#f7e73c';
        var size = 10;


        if(eq.magnitude >= 7.5) {
            color = '#ff0000';
            size = 50;
        } else if(eq.magnitude >= 6) {
            color = '#FFBD61';
            size = 25;
        }

        var marker = new google.maps.Circle({
          center: new google.maps.LatLng( eq.latitude, eq.longitude ),
          radius: size*1600,
          strokeColor: color,
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: color,
          fillOpacity: 0.35
        });

        marker.info = eq;

        google.maps.event.addListener(marker, 'click', function(e) {
            earthquake.displayOverlay(marker);
        });


        if(earthquake.checkEarthquake(marker)) {
            earthquake.eq.push(marker);
        } else {
            delete marker;            
        }

    }
};


$(document).ready(function() {

    earthquake.init();

});



if (typeof(console) == "undefined") { console = {}; }
if (typeof(console.log) == "undefined") { console.log = function() { return 0; } }


