<?php
ini_set('display_errors','On');
error_reporting(E_ALL ^E_NOTICE);
date_default_timezone_set  ( 'Europe/Vienna'  );

if( php_sapi_name() != "cli") die('direct!');

// SETTING
define('ROOT', dirname(__FILE__));
include(ROOT . "/config.php")


$xml    = simplexml_load_file("http://earthquake.usgs.gov/earthquakes/shakemap/rss.xml", null, LIBXML_NOCDATA);
$con    = new PDO("mysql:host=" . $host . ";dbname=" . $db . ";", $user, $password);
$i      = 0;
$over      = 0;
$whole      = 0;

echo "started: ".date("Y-m-d H:i")."\n";
foreach($xml->channel->item as $item) {	
	
    $sub    = (array) $item->children("http://earthquake.usgs.gov/rss/1.0/");
    $sub    += (array) $item->children("http://purl.org/dc/elements/1.1/");
    $sub    += (array) $item->children("http://www.w3.org/2003/01/geo/wgs84_pos#");

    $item   = (array) $item;
    $item   = (object) array_merge($item,$sub);
    
    $link = $item->link;
    $link = explode("/", $link);
    
    $original_id = $link[sizeof($link)-2];

	$date = $item->description;
	$date = substr($date, strpos($date, "<p>Date: ")+strlen("<p>Date: "));
	$date = substr($date, 0, strpos($date, "<br/>"));
    $date = new DateTime( $date );#$item->pubDate);
    $date = $date->format("Y-m-d H:i");

	$find   = $con->prepare("
		SELECT
			COUNT(`original_id`) cnt
		FROM `earthquake_0911`
		WHERE `original_id` = :original_id;
	");

    $find->bindValue(':original_id', $original_id );
    $find->execute();
    
    list($cnt) = $find->fetchAll(PDO::FETCH_OBJ);

    if($cnt->cnt == 0) {

        echo "   # insert eq '".$date."'\n";

        $eq   = $con->prepare("
            INSERT INTO `earthquake_0911` (
            	`original_id`,
                `title`,
                `datetime`,
                `latitude`,
                `longitude`,
                `depth`,
                `magnitude`,
                `added`
            ) VALUES (
            	?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?,
                ?
            );
        ");

        list($magnitude) = explode(" - ",$item->title);

		
        $eq->execute(array(
        	$original_id,
            $item->title,
            $date,
            number_format($item->lat ,5),
            number_format($item->long, 5),
            $item->depth,
            $magnitude,
            date("Y-m-d H:i")
        ));
		
        $eq->closeCursor();

        $i++;

    } else {
        #echo "   # overjumped ".$item->title."\n";
        $over++;
    }

    $whole++;


}
echo "\n   # inserted $i eqs\n   # overjumped $over eqs\n   # whole $whole eqs";

echo "\n\n---------------------\n";
?>

